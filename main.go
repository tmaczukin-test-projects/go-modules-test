package main

import (
	"github.com/sirupsen/logrus"
	"rsc.io/quote"
)

func HelloPrinter() {
	logrus.Println(quote.Hello())
}

func main() {
	HelloPrinter()
}
