module gitlab.com/tmaczukin-test-projects/go-modules-test

require (
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.2.2
	rsc.io/quote v1.5.2
)
