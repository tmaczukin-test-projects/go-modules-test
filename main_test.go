package main

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHelloPrinter(t *testing.T) {
	hook := test.NewGlobal()

	HelloPrinter()

	entry := hook.LastEntry()
	require.NotNil(t, entry)
	assert.Equal(t, logrus.InfoLevel, entry.Level)
	assert.NotEmpty(t, entry.Message)
}
